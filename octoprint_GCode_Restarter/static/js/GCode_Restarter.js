$(function() {
    function GCode_Restarter_ViewModel(parameters) {
        var self = this;

        self.controlViewModel = parameters[0];

        // this will hold the URL currently displayed by the iframe
        self.zheight = ko.observable();

        // this will hold the URL entered in the text field
        //self.newUrl = ko.observable();

        // this will be called when the user clicks the "Go" button and set the iframe's URL to
        // the entered URL
        //self.goToUrl = function() {
        //    self.currentUrl(self.newUrl());
        //};
        self.getheight() {
        	   self.zheight(self.zheight);
        };
        //self.jogyinc = function() {
        //    OctoPrint.printer.jog({"y", 1.0});
        //};
        //self.jogydec = function() {
        //    OctoPrint.printer.jog({"y", -1.0});
        //};
        //self.jogxinc = function() {
        //    OctoPrint.printer.jog({"x", 1.0});
        //};
        //self.jogxdec = function() {
        //    OctoPrint.printer.jog({"x", -1.0});
        //};
        //self.jogzinc = function() {
        //    OctoPrint.printer.jog({"z", 1.0});
        //};
        //self.jogzdec = function() {
        //    OctoPrint.printer.jog({"z", -1.0});
        //};
        //self.homexy = function() {
        //    OctoPrint.printer.home(["x", "y"]);
        //};
        //self.homez = function() {
        //    OctoPrint.printer.home(["z"]);
        //};
        //self.getAdditionalControls = function() {
        //    return [{
        //        'name': 'Reporting', 'children':[
        //            {'command': 'M114',
        //            'name': 'Get Position',
        //            'regex': 'X:([-+]?[0-9.]+) Y:([-+]?[0-9.]+) Z:([-+]?[0-9.]+) E:([-+]?[0-9.]+).+',
        //            'template': 'Position: X={0}, Y={1}, Z={2}, E={3}',
        //            'type': 'command'}
        //        ]
        //    }];
        //};

        // This will get called before the HelloWorldViewModel gets bound to the DOM, but after its
        // dependencies have already been initialized. It is especially guaranteed that this method
        // gets called _after_ the settings have been retrieved from the OctoPrint backend and thus
        // the SettingsViewModel been properly populated.
        //self.onBeforeBinding = function() {
        //    self.newUrl(self.settings.settings.plugins.GCodeRestarter.url());
        //    self.goToUrl();
        //}
    }

    // This is how our plugin registers itself with the application, by adding some configuration
    // information to the global variable OCTOPRINT_VIEWMODELS
    OCTOPRINT_VIEWMODELS.push({
        // This is the constructor to call for instantiating the plugin
        //GCodeRestarterViewModel,
        construct: GCode_Restarter_ViewModel,
        dependencies: [ "controlViewModel" ]
        // This is a list of dependencies to inject into the plugin, the order which you request
        // here is the order in which the dependencies will be injected into your view model upon
        // instantiation via the parameters argument
        //["settingsViewModel"],

        // Finally, this is the list of selectors for all elements we want this view model to be bound to.
        elements: ["#tab_plugin_GCodeRestarter"]
    });
});
