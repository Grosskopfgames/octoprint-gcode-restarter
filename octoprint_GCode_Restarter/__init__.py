########################################################################
#   Octoprint-GCodeRestarter
#   Copyright (C) 2020  Gerrit Großkopf
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#########################################################################

from __future__ import absolute_import, unicode_literals
import octoprint.plugin
import flask
import os
import os.path

class GCode_Restarter(#octoprint.plugin.StartupPlugin,
                     #octoprint.plugin.TemplatePlugin,
                     #octoprint.plugin.SettingsPlugin,
                     octoprint.plugin.AssetPlugin,
                     #octoprint.plugin.UiPlugin,
                     octoprint.plugin.EventHandlerPlugin,
                     octoprint.plugin.BlueprintPlugin
                     ):
    #def on_after_startup(self):
        #self._logger.info("Hello World! (more: %s)" % self._settings.get(["url"]))
    #def get_settings_defaults(self):
        #return dict(url="https://en.wikipedia.org/wiki/Hello_world")
    #def get_template_vars(self):
    #    return dict(url=self._settings.get(["url"]))
    #def get_template_configs(self):
    #    return [
    #        dict(type="navbar", custom_bindings=False),
    #        dict(type="settings", custom_bindings=False)
    #    ]
    def get_assets(self):
        return dict(
            js=["js/GCode_Restarter.js"],
            #css=["css/GCodeRestarter.css"]
        )
    def on_event(self, event_name, event_payload):
        if event_name=="PositionUpdate":
            self._logger.info("PositionUpdate")
            self._logger.info(event_payload["z"])
    @octoprint.plugin.BlueprintPlugin.route("/helloworld", methods=["GET"])
    def helloworld(self):
        return "Hello World"
    @octoprint.plugin.BlueprintPlugin.route("/listfile", methods=["GET"])
    def listFile(self):
        return "list of files: \n"+str(os.listdir(os.path.expanduser("~")+"/.octoprint/uploads/"))+"\n"
    @octoprint.plugin.BlueprintPlugin.route("/splitfile", methods=["GET"])
    def splitFile(self):
        if not "filename" in flask.request.values:
            return flask.make_response("Expected a filename and a height", 400)
        elif not "height" in flask.request.values:
            filestmp=os.listdir(os.path.expanduser("~")+"/.octoprint/uploads/")
            actfilename=""
            for filetmp in filestmp:
                if(filetmp.lower()==flask.request.values["filename"].lower()):
                    actfilename=filetmp
            return flask.make_response(actfilename,400)
        else:
            filestmp=os.listdir(os.path.expanduser("~")+"/.octoprint/uploads/")
            actfilename=""
            for filetmp in filestmp:
                if(filetmp.lower()==flask.request.values["filename"].lower()):
                    actfilename=filetmp
            with open(os.path.expanduser("~")+"/.octoprint/uploads/"+actfilename) as f:
                height=float(flask.request.values["height"])
                content = f.readlines()
                heights=[]
                actline=0
                for i in range(len(content)):
                    if(content[i]==";BEFORE_LAYER_CHANGE\n"):
                        if len(content)>i+2:
                            #print(content[i+2])
                            if(actline==0 and float(content[i+1][1:])>height):
                                actline=heights[-1][0]
                            heights.append((i,float(content[i+1][1:])))
                #print(heights)xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
                newcontent=[]
                if len(heights)==0:
                    return "GCode incompatible, set ';BEFORE_LAYER_CHANGE\n;[layer_z]' as Before layer change G-code in printersettings"
                for i in range(heights[0][0]):
                    if(content[i][:3]!="G80" and content[i][:3]!="G28" and content[i][:3]!="G1 "and content[i][:3]!="G92"):
                        newcontent.append(content[i])
                newcontent.append("G1 Z"+str(height)+"\n")
                for i in range(heights[0][0],actline):
                    if(content[i][:4]=="M104" or content[i][:4]=="M140"):
                        newcontent.append(content[i])
                for i in range(actline,len(content)):
                    newcontent.append(content[i])
                #print("".join(newcontent))
                if not os.path.exists(os.path.expanduser("~")+"/.octoprint/uploads/DANGER_GCode_RestarterPluginFiles/"):
                    os.makedirs(os.path.expanduser("~")+"/.octoprint/uploads/DANGER_GCode_RestarterPluginFiles/")
                fo = open(os.path.expanduser("~")+"/.octoprint/uploads/DANGER_GCode_RestarterPluginFiles/"+flask.request.values["filename"], "w+")
                fo.writelines(newcontent)
            return ""


__plugin_implementation__ = GCode_Restarter()
__plugin_pythoncompat__ = ">=2.7,<4"
__plugin_settings_overlay__ = dict(controls=[dict(children=[
        dict(command="M114",name="Update Height"),
        dict(
        javascript="height=data.output().split(' ')[1].split('m')[0];"+
            " (async function(){ fetch('./plugin/GCode_Restarter/splitfile\?filename\='+data.input[0].value()+'\&height\='+height); })();",#+
            #" (async function(){"+
            #    "var http = new XMLHttpRequest(); "+
            #    "http.open('POST', './api/files/local/DANGER_GCode_RestarterPluginFiles/'+data.input[0].value(), true);"+
            #    "http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');"+
            #    "http.send('print=true');"+
            #" })();",
        name="Split GCode at:",
        input=[dict(name="Filename", parameter="gcode",default="gcode123")],
        regex="X:([-+]?[0-9.]+) Y:([-+]?[0-9.]+) Z:([-+]?[0-9.]+) E:([-+]?[0-9.]+)",
        template="height: {2}mm"#.controlsFromServer[0].children alert('Slicing '+data.input[0].value()+' at '+data.output()); console.log(self.controlsFromServer[0].children); console.log(data);
        )],name="GCode Restarter")])
