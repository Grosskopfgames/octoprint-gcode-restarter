# GCodeRestarter

This plugin helps remove parts of the GCode of a selected file so that the file can be restarted from a certain height again.

## Setup

Install manually using this URL:

    https://gitlab.com/Grosskopfgames/octoprint-gcode-restarter/-/archive/master/octoprint-gcode-restarter-master.zip


## Configuration


## Usage

Make sure, your GCode is generated with ";BEFORE_LAYER_CHANGE" and ";[layer_z]" as the first two layers in the "Before layer change G-code" in Prusaslicer. This works out of the Box for Prusa Printers.

Move the Printhead down to the point where the Print failed that you want to restart.

You can do a scratchtest with a piece of Paper to make sure you are exactly ontop.

Press the "Update Height" Button, make sure the shown height makes sense and is not 0

Press the "Split GCode at:" Button

Reload the page

AT YOUR OWN RISK:

start the file from the DANGER_GCode_RestarterPluginFiles Folder
